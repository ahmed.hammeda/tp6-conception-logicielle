from typing import Optional

from fastapi import FastAPI
from pydantic import BaseModel
app = FastAPI()

class Mot(BaseModel):
    id:int
    caracteres:str

mot1= Mot(id=1,caracteres='oui')
mots=[mot1]

@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.get("/mots")
def get_mots():
    return mots

@app.get("/mot")
def add_mot(mot:Mot):
    return mots[0]

@app.post("/mot")
def add_mot(mot:Mot):
    mots.append(mot)
    return mots

@app.get("/items/{item_id}")
def get_items(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}
